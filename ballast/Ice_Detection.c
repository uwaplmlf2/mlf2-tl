/* Code fragment - ice detection logic - Removed from MIZ
 Nov 2015 */


/* structure to keep track of ice detection logic*/
// number of ice-condition "ecpochs"
// each ends on a particular day and has its own rules
// last one has just one rule: attempt to surface no matter what

# define N_ICE_EPOCH (3)
static struct ice
{
    long IntervalSec; // burst interval
    long DurationSec; // burst duration
    long DelaySec;		// delay before the first burst (after start of a mode, check against mode_time)
    double PatmUsed;			// Patm used by the altimeter, must match surfcheck.c!
    double Patm;			// Best guess of the actual Patm
    double IceReturnScale;	// Scaling used to return ice draft from surfcheck  (must match surfcheck.c!)
    
    double MaxPhotoP; // max depth to take photos
    
    double EpochYearDay[N_ICE_EPOCH]; // ending dates of each epoch;
    
    short NeedGreenCount[N_ICE_EPOCH]; // min. number of "green flags" to surface?
    short IgnoreBad[N_ICE_EPOCH]; // Ignore bad returns (otherwise, each one resets the GreenCount)
    short SurfaceEarly[N_ICE_EPOCH]; // whether to surface as soon as the surfacing rule is satisfied (otherwise let the mode time out)
    short act_now; // Flag set by the timer, indicates that the decision will be made based on the next run
    short okToSurface; // Latest verdict (remember to reset when in doubt!)
    short SurfcheckRequested; // Indicates that surfcheck was requested during the sampling cycle (1: altimeter only, 2: with photo. *Only used in simulator*)
    
    // other options...
    //	double IceMax[N_ICE_EPOCH]; // max. ice thickness to count as "green flag" (in conjunction with open-water flag or ice flag?
    //	double MinCommInterval[N_ICE_EPOCH]; // min. interval since the last comm
    
    short Run; // enable altimeter bursts (set in next_mode, hard-wired)
    double day_time_last; // save day_time of the previous burst
    int Count; // number of results since last reset (for information)
    int GreenCount; // number of consecutive "green" flags (reset at the start of the mode)
    
    int PhotosTaken; // # of photos taken in a current series
    short NeedPhoto; // indicates that a photo should be taken on the next run
} Ice;

