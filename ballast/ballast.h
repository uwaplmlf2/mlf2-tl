/*
 ** arch-tag: ballast header file
 ** Time-stamp: <2018-04-04 09:16:54 mike>
 **
 ** Parameters for MLF2 ballasting routine

 4/26/00 D'Asaro mods on Kenney version
 6/25/00
 Rewrite & simplify after first missions
 7/4/00 - close to final for July00
 mission
 8/16/00 - start mods for Fall00 ML mission
 6/5/02 - Hurricane
 3/1/02 - MIXED
 7/11/02 -CBLAST03
 2/2/04 - ADV float mods
 1/30/05 - Equatorial mods
 4/28/06 - AESOP mods and cleanup
 9/21/06 - DOGEE Gasfloat mods
 11/16/06 - Equatorial Nightbug mods and update
 11/14/07 -NAB changes in Targetting parameters
 11/10/10 - Eliminate eos structure, Add Error structure
 9/10/11 - Add StageSim structure
 5/21/12  - "Global timer" functionality added (AS)
 5/21/12  - Second UP mode added (AS)
 1/31/14 - New sensor macros and formatting cleanup
 2/12/14 - Multi-day timers, fix profile saving overrun
 2/12/15 - BOCHA Mode
 2/26/15 - SETTLE_P Mode
 3/30/15 - Rename BOCHA - > SERVO_P mode
 3/30/15 - New BOCHA (B=const) mode
 11/26/15 - New macros to add parameters to Ptable in a uniform and streamlined manner (AS)
 06/07/16 - New structures and functions for EOS
 Aug 2018 - Minor changes to
*/
#undef SIMULATION

#ifndef _BALLAST_H_     /* check that only one definition is included */
#define _BALLAST_H_

#ifndef SIMULATION
#include "config.h"
#else // SIMULATION
/* __________  next section only for Matlab use __________*/
#include "ptable_mex.h"
// In real code, INITFUNC macro is defined in config.h with the  "constructor" attribute (GCC thing). 
// For Matlab simulation we need to define & call manually, so...
#define INITFUNC(name) void name(void)
#endif

enum {MODE_PROFILE_DOWN=0,
      MODE_SETTLE,
      MODE_PROFILE_UP,
      MODE_DRIFT_ISO,
      MODE_DRIFT_SEEK,
      MODE_DRIFT_ML,
      MODE_SERVO_P,
      MODE_BOCHA,
      NR_REAL_MODES,
      MODE_GPS,
      MODE_COMM,
      MODE_ERROR,
      MODE_DONE,
      MODE_START,
      MODE_SLEEP,
      MODE_XFER};

#define MODE_RESTORE (-1) /* pass this to next_mode to restore the previous mode */
#define MODE_INIT (-10) /* last_mode is initialized with MODE_INIT to signify the first entrance into the mlf2_ballast routine */

#define is_profile(m)   ((m) == MODE_PROFILE_DOWN ||    \
                         (m) == MODE_PROFILE_UP )
#define is_drift(m)     ((m) == MODE_DRIFT_ISO ||       \
                         (m) == MODE_DRIFT_ML ||        \
                         (m) == MODE_DRIFT_SEEK ||      \
                         (m) == MODE_SERVO_P ||         \
                         (m) == MODE_BOCHA)
#define is_settle(m)    ((m)==MODE_SETTLE )


/* these macros abbreviate Ptable addition statements (Ptable name matches C variable name)*/
# define ADD_PTABLE_D(v) add_param(#v,PTYPE_DOUBLE,&v)
# define ADD_PTABLE_S(v) add_param(#v,PTYPE_SHORT,&v)
# define ADD_PTABLE_L(v) add_param(#v,PTYPE_LONG,&v)

# define ADD_PTABLE__D(s,v) add_param(#s"."#v,PTYPE_DOUBLE,&(s.v))
# define ADD_PTABLE__S(s,v) add_param(#s"."#v,PTYPE_SHORT,&(s.v))
# define ADD_PTABLE__L(s,v) add_param(#s"."#v,PTYPE_LONG,&(s.v))


/*
** Global variables for mode control
*/
extern unsigned long Sensors[];
extern short Si[];

#define ENABLE_SENSORS(m, slist)                \
    do { if((m) >= 0 && (m) < NR_REAL_MODES)    \
            Sensors[(m)] |= (slist);            \
    } while(0)

#define DISABLE_SENSORS(m, slist)               \
    do { if((m) >= 0 && (m) < NR_REAL_MODES)    \
            Sensors[(m)] &= ~(slist);           \
    } while(0)

/*
** Sensor types
*/
#if (HAVE_CTD & 1)
#  define SENS_CTD1     0x001
#else
#  define SENS_CTD1     0x0
#endif
#if (HAVE_CTD & 2)
#  define SENS_CTD2     0x002
#else
#  define SENS_CTD2     0x0
#endif
#ifdef HAVE_ADCP
#  define SENS_ADCP     0x004
#else
#  define SENS_ADCP     0x0
#endif
#ifdef HAVE_BB2F
#  define SENS_BB2F 0x008
#else
#  define SENS_BB2F 0x0
#endif
#ifdef HAVE_ALTIMETER
# define  SENS_ALT      0x010
#else
# define  SENS_ALT      0x0
#endif
#ifdef HAVE_PAR
#  define SENS_IPAR     0x020
#else
#  define SENS_IPAR     0x0
#endif
#ifdef HAVE_NOISE
#  define SENS_NOISE    0x040
#else
#  define SENS_NOISE    0x0
#endif
#ifdef HAVE_CSTAR
#  define SENS_CSTAR      0x100
#else
#  define SENS_CSTAR      0x0
#endif
#if defined(HAVE_FLUOROMETER) || defined(HAVE_ECO)
#  define SENS_FLR      0x200
#else
#  define SENS_FLR      0x0
#endif
#ifdef HAVE_I490
#  define SENS_I490     0x400
#else
#  define SENS_I490     0x0
#endif
#ifdef HAVE_CTDO
#  define SENS_CTDO     0x800
#  define SENS_O2       0x200000L
#else
#  define SENS_CTDO     0x0
#  define SENS_O2       0x0
#endif

#ifdef HAVE_TCHAIN
#  define SENS_TCHAIN    0x1000
#else
#  define SENS_TCHAIN    0x0
#endif

#ifdef HAVE_THERM
#  define SENS_THERM    0x2000
#else
#  define SENS_THERM    0x0
#endif

/* Bio-Heavy sensors (managed separately) */
#ifdef HAVE_BIOSENSORS
#define HAVE_OPTODE
#define HAVE_FLNTU
#endif

/* Optode */
#ifdef HAVE_OPTODE
#define SENS_OPTODE     0x08000L
#else
#define SENS_OPTODE     0x0
#endif

/* FLNTU */
#ifdef HAVE_FLNTU
#define SENS_FLNTU      0x04000L
#else
#define SENS_FLNTU      0x0
#endif

/* Acoustic Noise Recorder */
#ifdef HAVE_ANR
#define SENS_ANR        0x100000L
#else
#define SENS_ANR        0x0
#endif

#ifdef HAVE_SSAL
#define SENS_SSAL_CAL   0x400000L
#else
#define SENS_SSAL_CAL   0x0
#endif

#ifdef HAVE_SBE63
#define SENS_SBE63        0x800000L
#else
#define SENS_SBE63        0x0
#endif

#ifdef HAVE_QCP
#define SENS_QCP        0x1000000L
#else
#define SENS_QCP        0x0
#endif

#ifdef HAVE_MCP
#define SENS_MCP        0x2000000L
#else
#define SENS_MCP        0x0
#endif

#ifdef HAVE_ECOPAR
#define SENS_ECOPAR     0x4000000L
#else
#define SENS_ECOPAR     0x0
#endif

#ifdef HAVE_GTD2
#define SENS_GTD2       0x8000000L
#else
#define SENS_GTD2       0x0
#endif

#ifdef HAVE_SUNA
#define SENS_SUNA       0x10000000L
#else
#define SENS_SUNA       0x0
#endif

#ifdef HAVE_CAMERA
#define SENS_CAMERA       0x20000000L
#else
#define SENS_CAMERA       0x0
#endif

#ifdef HAVE_TRIOS
#define SENS_TRIOS       0x40000000L
#else
#define SENS_TRIOS       0x0
#endif

#define SENS_PAR    (SENS_IPAR | SENS_I490 | SENS_QCP | SENS_MCP |      \
                     SENS_ECOPAR)
#define SENS_CTD        (SENS_CTD1 | SENS_CTD2)


/*
** Allowed sensors in each mode.
*/
#define PROF_SENSORS    (SENS_CTD | SENS_CTDO | SENS_ADCP | SENS_THERM | \
                         SENS_PAR | SENS_FLR | SENS_CSTAR | SENS_GTD2 |   \
                         SENS_ANR | SENS_O2 | SENS_SBE63 | SENS_SUNA |    \
                         SENS_OPTODE | SENS_FLNTU | SENS_TRIOS | SENS_TCHAIN)
#define SETTLE_SENSORS  (PROF_SENSORS)
#define DRIFT_SENSORS   (PROF_SENSORS)

/* MODE_ERROR type codes */
typedef enum {
    ERR_NONE=0,
    ERR_UNKNOWN,
    ERR_INVALID_MODE,
    ERR_INVALID_STAGE,
    ERR_INVALID_ICALL,
    ERR_PRESSURE,
    ERR_PRESSURE_RANGE,
    ERR_PRESSURE_SENSOR,
    ERR_BAD_CTD,
    ERR_ZERO_RHO,
    ERR_COMM_OVERDUE,
    ERR_PISTON,
    ERR_MOTOR_STALL,
    ERR_MOTOR_LIMIT,
    ERR_FILE_OPEN,
    ERR_FILE_WRITE,
    ERR_BATTERY12,
    ERR_BATTERY15,
    ERR_HUMIDITY,
    ERR_AUX
} error_t;

int handle_mode_error(error_t err);

/* function prototypes */

int next_mode(int mode, double day_time);
int sampling(int nmode, double day_time, double day_sec, double mode_time);
int setup(void); // Set up parameters

int check_timers(int mode, double day_time, double daysec, double last_daysec, int *mode_out);// Check the global timers

double sw_dens(double S, double T, double P);
double sw_pden(double S, double T, double P, double PR);
double sw_ptmp(double S,double T,double P,double PR);
double sw_temp(double S,double T,double P,double PR);
double  sw_adtg(double S,double T, double P);
double sw_dens0(double S, double T);
double filtmean(double *X, int N);
double opt_med5(double *p);
void mlf2_ballast(double day_time, double Pressure, double Temperature,
                  double Salinity,double Temperature_2, double Salinity_2,
                  double ballast, int mode,
                  int drogue_in, double daysec, int command,
                  double *B, int *mode_out,int *drogue_out, double *telem_out);
void safe_ballast(double day_time, double Pressure,double ballast, int mode,
                  double *B,int *mode_out,int *drogue_out);
void labsim(double day_time, int mode, double *Pressure,
            double *Temperature, double *Salinity);
void labsimtest(double day_time, int mode,
                double T, double S, double P,
                double B,int diag,
                double *Pressure, double *Temperature, double *Salinity);
// There are a few more EOS-handling function prototypes after EOS structure definitions


/* Handling Timer & Command exceptions*/
/* These functions need to be supplied by [mission].c */
/* Each function returns
   0 if Ballast.c can proceed normally
   -1 if immediate return is needed
   1 if the current mode needs to terminate gracefully (mode is set to new_mode, and next_mode is called prior to return)

*/
int handle_timer(int mode, double day_time, int timer_command, int *mode_out);
int handle_command(int mode, double day_time, int command, int *mode_out);

#ifndef SIMULATION
#include <time.h>

short daytime(time_t sampleTime,
              float lon,
              float lat,
              long amoffset,
              long pmoffset,
              long darkSamplePeriod);

/* Photo management functions */
int camera_keep_photo(void);
void camera_store_photos(int n);
int get_surfcheck_result(void);
#else
/* No-op for the simulator */
#define camera_keep_photo()  (0)
#define camera_store_photos(n)
#define get_surfcheck_result() (-1)
#endif

void * ssal_start_profile(long timeout);
int ssal_stop_profile(void *obj);

/* Mission Parameters
   Structures - organized by Mode  */


/* UNIFIED UP/DOWN PROFILE*/
/*  Several modes of operation
 
FAST - deltaB>Bmax: use maximum bocha

CONTROLLED - seeks W-  Computes equilibrium ballast (W=0) based on EOS
 
    HARD - offset ballast by deltaB relative to equilibrium
           Set W=0 to use
 
    SOFT -
        if abs( Wmeasured - W) < Prof.Wband
            Proportional control at rate Tau
            Uses DRAG law and EOS
        else same as HARD
 */
struct profile {
	short Dir;			// Profile direction: 1=UP, -1=DOWN
	double timeout;		// Max duration 
	double Ptarget;		// Pressure to end the profile at
	double ExtraTime;	// time/sec past reaching Prof.Ptarget before end

	double deltaB;		// Bocha offset relative to the local equilibrium (proxy for profiling speed). 
						// Good numbers will probably be +/- 10e-6 to 100e-6
						// Setting it to +1 is equivalent to the old UP, -1 - to the old DOWN
						
	double W;			// Profile speed Target, positive upward [m/s]
						// if non-zero, supersedes deltaB
	double Tau;			// Time constant to be used in velocity control [s]
	double WcontrolPmin; // Min depth for W control [m]. Above this depth use deltaB.
	double Wband;		// W control band [fractional]. I.e. use "hard" (deltaB) control outside W*[1-Wband,1+Wband]. Default Wband=1
	double Extrap;		// Extrapolation parameter for predicting B0. =0 disables extrapolation, =1 extrapolates to the next data cycle, =0.5 to half-way

// May want to add tapering parameter(s) here -- by time (as in Settle) or by pressure window
	// double DecayWindow

	double Bmin;		// min bocha setting
	double Bmax;		// max bocha setting

	short  drogue;		// drogue open (1) or closed (0) during the profile
	short  Speedbrake;  // if 1, use drogue as speedbrake until float moves in the right direction
};

/* macro for adding a group of "Profile" parameters to a Ptable
Use it like this:
ADD_PTABLE_PROFILE(Down);
or
ADD_PTABLE_PROFILE(Steps.Down[0]);
de
This will ensure that all the parameters are added uniformly and none are lost.
The macro is not universally safe, but should be fine if used as intended (see above).
A correct way would be to use do{...}while(0) (as Mike does in ENABLE_SENSORS)
- AS
*/
# define ADD_PTABLE_PROFILE(v) {\
	ADD_PTABLE__S(v, Dir);	\
	ADD_PTABLE__D(v, timeout);	\
	ADD_PTABLE__D(v, Ptarget);	\
	ADD_PTABLE__D(v, ExtraTime);	\
	ADD_PTABLE__D(v, deltaB);	\
	ADD_PTABLE__D(v, W);	\
	ADD_PTABLE__D(v, WcontrolPmin);	\
	ADD_PTABLE__D(v, Tau);	\
	ADD_PTABLE__D(v, Wband);	\
	ADD_PTABLE__D(v, Extrap);	\
	ADD_PTABLE__D(v, Bmin);	\
	ADD_PTABLE__D(v, Bmax);	\
	ADD_PTABLE__S(v, drogue);	\
	ADD_PTABLE__S(v, Speedbrake);	\
};
// Macros for deciding whether the profile is going up or down (they also have different mode IDs, but I don't rely on that!)
#define PROF_UP (Prof.Dir>0)
#define PROF_DOWN (Prof.Dir<0)



/* SERVO_P MODE parameters */
struct servo_p {
    double Pmin;    /* mode ends if shallower than this */
    double Pmax;    /* mode ends if deeper than this */
    double Pgoal;   /* push out Bocha if shallower */
    double Bspeed;  /* bocha speed when below Pgoal, m^3/s */
    double BspeedEtime; /* efolding time to decrease Bspeed (sec) */
    double BspeedShallow;  /* bocha speed when above Pgoal, m^3/s, SHOULD BE NEGATIVE! */
    double timeout; /* Max duration */
    short  drogue;    /* 0 or 1 - drogue open or closed  */
};
/* macro for adding a group of "Servo_p" parameters to a Ptable*/
# define ADD_PTABLE_SERVO_P(v) {\
	ADD_PTABLE__D(v,Pmin);	\
	ADD_PTABLE__D(v,Pmax);	\
	ADD_PTABLE__D(v,Pgoal);	\
	ADD_PTABLE__D(v,Bspeed);	\
	ADD_PTABLE__D(v,BspeedEtime);	\
	ADD_PTABLE__D(v,BspeedShallow);	\
	ADD_PTABLE__D(v,timeout);	\
	ADD_PTABLE__S(v,drogue);	\
}


/* SETTLE MODE */
struct settle {
    double secOday;   /* end Settle when time passes this clock time [0 86400] */
    double timeout;         /* Max time of settle-leg/sec -  Skip settle if negative*/
    double seek_time;     /* time for active seeking, if seek_time<1, then a fraction of timeout  */
    double decay_time;    /* decay seeking over this time scale after seek_time, if decay_time<1, then a fraction of seek_time  */
    short  nav;         /* number of points to average to get settled volume */
    double drogue_time; /* time after start of settle to keep drogue open */
    double beta; /* pseudo compressive gain*/
    double tau;             /* seek gain */
    double weight_error;    /* Accept seek values if Dsig*V0 is less than this */
    double Vol_error;       /* Accept volume if stdev of V0 is less than this ( if <0 no volume set) */
    double Ptarget;      /* Target Pressure - used only with Nfake */
    double Nfake;           /* Fake stratification relative to Ptarget  No effect if 0 */
    short  nskip;           /*  (if 0, skip settle mode always)  */
    short SetTarget; /* How to set target
    1- current PotDensity,2-Ballast.target,3-Drift.target, else constant*/
    double Target;          /* Settle Target isopycnal */
    double B0;              /* bocha */
    double Bmin; /* Minimum Bocha */
    double Bmax; /* Maximum Bocha */
	short SetV0; /* if ==1, set EOS.V0 after successful settle */
};
/* macro for adding a group of "settle" parameters to a Ptable*/
# define ADD_PTABLE_SETTLE(v) {\
	ADD_PTABLE__D(v,secOday);	\
	ADD_PTABLE__D(v,timeout);	\
	ADD_PTABLE__D(v,seek_time);	\
	ADD_PTABLE__D(v,decay_time);	\
	ADD_PTABLE__S(v,nav);	\
	ADD_PTABLE__D(v,drogue_time);	\
	ADD_PTABLE__D(v,beta);	\
	ADD_PTABLE__D(v,tau);	\
	ADD_PTABLE__D(v,weight_error);	\
	ADD_PTABLE__D(v,Vol_error);	\
	ADD_PTABLE__D(v,Ptarget);	\
	ADD_PTABLE__D(v,Nfake);	\
	ADD_PTABLE__S(v,nskip);	\
	ADD_PTABLE__S(v,SetTarget);	\
	ADD_PTABLE__D(v,Target);	\
	ADD_PTABLE__D(v,B0);	\
	ADD_PTABLE__D(v,Bmin);	\
	ADD_PTABLE__D(v,Bmax);	\
	ADD_PTABLE__S(v,SetV0);	\
}


/* FIXED BOCHA MODE*/
struct bocha {
    double B; /* Constant bocha to set*/
    double Btol; /* Tolerance of Bocha. Set to -1 to disable exit-on-target-B */
    double timeout; /* Max duration */
    short  drogue;    /* 0 or 1 - drogue open or closed  */
	short SetV0; /* if ==1, set EOS.V0 after successful settle */
};
/* macro for adding a group of "settle_p" parameters to a Ptable*/
# define ADD_PTABLE_BOCHA(v) {\
	ADD_PTABLE__D(v,B);	\
	ADD_PTABLE__D(v,Btol);	\
	ADD_PTABLE__D(v,timeout);	\
	ADD_PTABLE__S(v,drogue);	\
	ADD_PTABLE__S(v,SetV0);	\
}

/* DRIFT MODE  */
struct drift {
    short int SetTarget; /*  How to set Target:
                             1- current value, 2-Ballast.target, 3-Settle.target, else constant*/
    short int VoffZero;  /* 1 to set Voff=0 at drift start, else keep value */
    short int median;  /* 1 use 5 point median filter, 0 don't */
    short int timetype; /* How to end Drift mode
                           1: Use time since end of last drift mode or mission start
                           2: At the given timeOday (defaults to noon = 0.5)
                           Also use timeout */
    double time_end_sec;   /*time/seconds to end Drift mode */
    double timeout_sec;    /* Additional timeout to end mode / seconds*/
    double Voff;       /* Ballast adjustment during Drift  */
    double Voffmin;  /*prevent negative runaway on bottom */
    double Target;         /* target isopycnal */
    double iso_time;        /*seek time toward surface  sec  */
    double seek_Pmin;      /* depth range for drift seek mode*/
    double seek_Pmax;      /* continued */
    double iso_Gamma;/*Pseudo-compressibility  m^3/unit*/
    double time2;  /* second timeout parameter ??? */
    double closed_time;   /* drogue opens after this time   sec  */
	short SetV0; /* if ==1, set EOS.V0 after the drift */
};
/* macro for adding a group of "drift" parameters to a Ptable*/
# define ADD_PTABLE_DRIFT(v) {\
	ADD_PTABLE__S(v,SetTarget);	\
	ADD_PTABLE__S(v,VoffZero);	\
	ADD_PTABLE__S(v,median);	\
	ADD_PTABLE__S(v,timetype);	\
	ADD_PTABLE__D(v,time_end_sec);	\
	ADD_PTABLE__D(v,timeout_sec);	\
	ADD_PTABLE__D(v,Voff);	\
	ADD_PTABLE__D(v,Voffmin);	\
	ADD_PTABLE__D(v,Target);	\
	ADD_PTABLE__D(v,iso_time);	\
	ADD_PTABLE__D(v,seek_Pmin);	\
	ADD_PTABLE__D(v,seek_Pmax);	\
	ADD_PTABLE__D(v,iso_Gamma);	\
	ADD_PTABLE__D(v,time2);	\
	ADD_PTABLE__D(v,closed_time );	\
	ADD_PTABLE__S(v,SetV0);	\
}

/* EOS  Float Equation of State variables */
struct eos {
	double Mass0;  /* initial mass */
	double V0;      /* Estimated float volume at zero bocha, at surface, ref Temp, no air */
	double Air;      /*  Air volume at the surface (m^3) */
	double Compress;  /* float compressibility (dbar^-1)*/
	double Thermal_exp;     /* float thermal expansion coeff */
	double Tref;      /* reference temperature */
	double Creep;  /* kg/day */
	double CreepStart; /* Days since the start of the mission to start the Creep */
	double Pair;     /* Atmospheric pressure - nominally 10 (dbar)*/
	// The following is set internally (not in PTable)
	double mass;    /* current float mass -corrected for creep and bugs */
};
# define ADD_PTABLE_EOS(v) {\
ADD_PTABLE__D(v,Mass0);	\
ADD_PTABLE__D(v,V0);	\
ADD_PTABLE__D(v,Air);	\
ADD_PTABLE__D(v,Compress);	\
ADD_PTABLE__D(v,Thermal_exp);	\
ADD_PTABLE__D(v,Tref);	\
ADD_PTABLE__D(v,Creep);	\
ADD_PTABLE__D(v,CreepStart);	\
ADD_PTABLE__D(v,Pair);	\
}

/* DRAG  Float drag law variables */
/* The drag law is DragForce = -(0.5*Cd*Area*W*|W| + Area*L*N*W)*Density 
	in terms of volume anomaly necessary to move at the vertical velocity W: VolOffset =  (0.5*Cd*Area*W*|W| + Area*L*N*W)/g

	g=9.81 m/s^2 is assumed
	*/
struct drag {
	double Cd;	// Drag coefficient [non-dimensional]
	double Area; // Cross-section area [m^2] * need to figure out what to do with the drogue
	double L;	 // Length [m] (not necessarily the same as float length)
	double Tau;	 // Time constant to be used in velocity control [s]
};
# define ADD_PTABLE_DRAG(v) {\
ADD_PTABLE__D(v,Cd);	\
ADD_PTABLE__D(v,Area);	\
ADD_PTABLE__D(v,L);	\
}

/* function defined after definition */

double get_bocha(double P, double T, double rho, double weight); // Compute bocha position needed to acheive given float weight in water. Neutral buoyancy(equilibrium bocha or B0) is a particular case (weight = 0)
double get_V0(double P, double T, double rho, double bocha); // computes V0 assuming float neutral
double get_Vdrag(double W, double N); // find volume anomaly given the vertical velocity and the drag law

/*  BALLAST  VARIABLES  */
struct ballast{
    short  SetTarget;  /* how to set Ballast.Target
                          1,2,3-no changes,    4- end of good Settle
                          5 - end of Drift, 6-end of Down, 7-end of Up,
                          8- Value at P=Ballast.Pgoal
                          9- Value from GetMLB (not implemented yet) */
    short MLsigmafilt;   /* 1 to use LowPass filtered Sigma in ML, else Ballast.rho0 */
    double MLthreshold;   /* Switch to ML ballasting if P<Pdev*     */
    double MLmin;            /*        or if P<     */
    double MLtop;          /* or between top and bottom */
    double MLbottom;
    double SEEKthreshold;   /* Allow isoSEEK if P>Pdev*  */
    double T0;      /* most recent temperature at ballasted point */
    double S0;      /* most recent Salinity  */
    double P0;      /* most recent Pressure*/
    double rho0;    /* most recent water (=float) density */
    double B0;      /* most recent bocha setting at ballasted point */
    double TH0;     /* most recent potential temperature */
    double Vdev;  /* most recentstdev of float volume estimates */
    double Pgoal;  /* if STP==2, set STP at this depth */
    double Target; /* target isopycnal e.g. 1022 */
};
/* macro for adding a group of "ballast" parameters to a Ptable
This may be an overkill, since there's likely to be only one Ballast structure, but this way everything is in one place...
*/
# define ADD_PTABLE_BALLAST(v) {\
	ADD_PTABLE__S(v,SetTarget);	\
	ADD_PTABLE__S(v,MLsigmafilt);	\
	ADD_PTABLE__D(v,MLthreshold);	\
	ADD_PTABLE__D(v,MLmin);	\
	ADD_PTABLE__D(v,MLtop);	\
	ADD_PTABLE__D(v,MLbottom);	\
	ADD_PTABLE__D(v,SEEKthreshold);	\
	ADD_PTABLE__D(v,T0);	\
	ADD_PTABLE__D(v,S0);	\
	ADD_PTABLE__D(v,P0);	\
	ADD_PTABLE__D(v,rho0);	\
	ADD_PTABLE__D(v,B0);	\
	ADD_PTABLE__D(v,Vdev);	\
	ADD_PTABLE__D(v,Pgoal);	\
	ADD_PTABLE__D(v,Target);	\
}

/*  CTD  VARIABLES */
#define BOTTOMCTD   0
#define TOPCTD      1
#define MEANCTD     2
#define MAXCTD      3
struct ctd{
    short which;  /* How to get one CTD value from two CTD's see
                     options are set above */
    short  BadMax;    /*How many bad CTDs before error */
    double Ptopmin;      /* TopCTD not good above this pressure */
    double Poffset;      /* How much deeper is the float's center relative to the (primary) pressure sensor? [dbar] */
    double Separation;  /* Distance between two CTDs [dbar] *** OBSOLETE? *** */
    double TopSoffset;    /* Correction to top CTD salinity */
    double TopToffset;   /*Correction to top CTD temperature*/
	double TopPoffset;   /*Top CTD pressure relative to the primary pressure sensor, typically<0 [dbar]*/
    double BottomSoffset;   /* Correction to Bottom CTD salinity */
    double BottomToffset;   /*Correction to Bottom CTD temperature*/
	double BottomPoffset;   /*Bottom CTD pressure relative to the primary pressure sensor, typically>0 [dbar]*/
};
# define ADD_PTABLE_CTD(v) {\
ADD_PTABLE__S(v,which);	\
ADD_PTABLE__S(v,BadMax);	\
ADD_PTABLE__D(v, Ptopmin);	\
ADD_PTABLE__D(v, Poffset);	\
ADD_PTABLE__D(v, Separation);	\
ADD_PTABLE__D(v, TopSoffset);	\
ADD_PTABLE__D(v, TopToffset);	\
ADD_PTABLE__D(v, TopPoffset);	\
ADD_PTABLE__D(v, BottomSoffset);	\
ADD_PTABLE__D(v, BottomToffset);	\
ADD_PTABLE__D(v, BottomPoffset);	\
}


/*  MIXED LAYER BASE STRUCTURE  */

#define Nsave 1000  /* array size for raw profile data */
#define Ngrid 100  /* for gridded data */
struct mlb{
    short  go;  /* 1 to compute MLB target, otherwise don't */
    short record;   /* 1 to record data in arrays; else don't */
    short point;       /* points at next open element in raw arrays, 0-> empty */
    short Nmin;    /* minimum number of data to do computation */
    double dP;     /* grid spacing m */
    double dSig;  /* bin size for Sigma search */
    double Sigoff; /* Offset of goal from ML density */
    double SigStep;  /* density step up/down from goal */
    double Psave[Nsave]; /* array of raw pressure */
    double Sigsave[Nsave]; /* array of raw potential density */
    double Pgrid[Ngrid]; /* array of gridded pressure */
    double Siggrid[Ngrid]; /* array of gridded potential density */
};

/* function declared after definition */
double getmlb(struct mlb *);  /* pass pointer to structure auto */
double z2sigma(struct mlb *,double Z);  /* pass pointer to structure auto */

/* BUTTERWORTH FILTER STRUCTURE */
struct butter {
    double Tfilt;  /* filter time - coeff should be consistent */
    double A2; double A3; /* Y coeff A1=1 */
    double B1;  double B2; double B3;/* X coeff */
    double Xp;    /* remember previous values */
    double Xpp;
    double Yp;
    double Ypp;
};
/* function declaration */
double Bfilt(struct butter * , double , double, short , struct butter );
double ButterLowCoeff(double, struct butter *);
double ButterHiCoeff(double, struct butter  *);

/*  DEPTH ERROR PARAMETERS */
struct error{
    short Modes;      /* 1: drift only   2: settle only 3: drift and settle   Else: None */
    double Pmin;      /* Minimum Pressure  allowed */
    double Pmax;     /* Maximum Pressure  allowed */
    double timeout;    /* time outside of this range before error is declared*/
};

/* BUG MITIGATION PARAMETERS */
struct bugs{
    double start;  /* sunset  / seconds of GMT day 0-86400 */
    double stop;  /* sunrise */
    double start_weight;  /* sunset weight / kg  */
    double stop_weight;  /* sunrise weight /kg - linear interpolation between */
    double flap_interval;  /* time between flaps / seconds */
    double flap_duration;  /* time between close and open */
    double weight;
};



//
///* Stage simulation */
//struct stagesim{
//  short jump;   /* change stage after this many COMMS */
//  short stage1; /* change stage to this first  */
//  short stage2;  /* second */
//  short count;  /* counts COMMS  */
//};


/* GLOBAL TIMERS */
#define NTIMERS 4
struct timer{
    short enable;   // ==1 if all timers are enabled (individual timers can be controlled by setting the time ore stage to -1
    double time[NTIMERS];   // time (in seconds since midnight) for the timer event.
    short command[NTIMERS];   // Timer command to execute when the timer is triggered (Reaction is determined by handle_timer() function, but command=-1 should do nothing)
    short nskip[NTIMERS];  // skip this many timer triggers - allows multiday timers
    // private: 
	short countdown[NTIMERS]; // counts down skips
};

/*
 END STRUCTURES */



#endif /* _BALLAST_H_ */
