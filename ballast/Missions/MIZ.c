/* arch-tag: 0e0de6c5-8b15-4b25-8b35-21d785e9076f */
/* FROM ORCA with other stuff added back in since ORCA is so simple */
/* NEED TO SET PARAMETERS SET IN InitializeMIZ */
/* FEB, 2015  */

# include <time.h> 

#define NSTAGES 4/* Number of mission stages 0..NSTAGES-1*/

#define max(a, b)	((a) > (b) ? (a) : (b))

/* Process the new surfcheck result and check the surfacing rules */
/* Will set Ice.ok_to_surface and return 1 if the mode needs to be terminated ASAP */
short check_surfacing_rules(double day_time, int surfcheck_result)
{
	time_t now;
	struct tm  *tm;
	double yday;
	int ie; // ice epoch
	double ice_draft = 100.;
	short surface_flag = -1; // -1: uncertain, 0: open water, 1: ice
	
	if (!Ice.Run)
	{ // shouldn't happen! This probably means the result arrived late
		// log an exception and ignore
		log_event("Ice [%d] unexpected\n", surfcheck_result);
		return 0;
	}
	// Get real time:
	time(&now);
	tm = gmtime(&now);
	yday = (double)tm->tm_yday + (double)tm->tm_hour / 24.0 + (double)tm->tm_min / 24.0 / 60.0 + (double)tm->tm_sec / 24.0 / 60.0 / 60.0;
	// find the ice "epoch" (0..N_ICE_EPOCH-1) corresponding to yday
	for (ie = 0; ie < N_ICE_EPOCH && yday > Ice.EpochYearDay[ie]; ie++){};
		
	// Process the new surfcheck result
	if (surfcheck_result>0) 
	{ // not an error
		surface_flag = surfcheck_result & 1;
		ice_draft = (double)(surfcheck_result >> 1) / Ice.IceReturnScale+Ice.Patm-Ice.PatmUsed;
	}
	Ice.Count++;
	
	if (ie == N_ICE_EPOCH)
	{
		Ice.okToSurface = 1;
		log_event("Ice [%d] d:%.2f f:%d ie:%d * Always surface *\n", surfcheck_result, ice_draft, surface_flag, ie);
		return 1;
	}

		/* Check the rules:	*/
	if (surface_flag==0)
	{
		// open water return is always a green flag
		Ice.GreenCount++;
			}
	if (surface_flag>0)
	{ // ice return = red flag (for now, may want to check thickness)
		// reset green flag counter
		Ice.GreenCount = 0;
			}
	if (surface_flag<0 && !Ice.IgnoreBad[ie])
	{ // this was a bad return in epoch when they were not ignored = red flag
		// reset green flag counter
		Ice.GreenCount = 0;
		
	}
	
	/* Final verdict: */
	Ice.okToSurface = (Ice.GreenCount >= Ice.NeedGreenCount[ie]);
	log_event("Ice [%d] d:%.2f f:%d ie:%d a:%d (%d/%d) %s\n", 
		surfcheck_result, ice_draft, surface_flag, ie, Ice.act_now, Ice.GreenCount, Ice.Count, (Ice.okToSurface) ? "* Ok to surface *" : "*  No  *");

	// now, return "1" if the current mode needs to be terminated:
	return Ice.act_now || (Ice.okToSurface && Ice.SurfaceEarly[ie]);

}

/* Reset ice okToSurface and GreenCount
   Also, disable Ice.Run (need to enable explicitly)
*/
void  ice_reset()
{
	Ice.okToSurface = 0;
	Ice.act_now = 0;
	Ice.Count = 0;
	Ice.GreenCount = 0;
	Ice.day_time_last = -1;
	Ice.Run = 0;
    Ice.PhotosTaken = 0;
    Ice.NeedPhoto = 0;
}



int next_mode(int nmode, double day_time)
{
    static int icall=0;
        
    static int oldstage=0;
    static int savemode; /* saved value of mode - except error */
    static int savetimer; /* saved value of Timer.enable */
    static int saveVset= -999; /* save value of Ballast.Vset */
    static int commskipcount=0;  /* Counts skipped comm modes */
    static int goodcomm=1;  /* flag to set if this program set COMM mode (as opposed to the emergency COMM after ERROR)*/
    static double hometime=0; /* time of last home */
    static double sleeptime=0;/* time of last sleep start */
    static double Brate;
    
	time_t now;
	struct tm  *tm;
	double yday;
	

	int oldmode, i, istep;
    double x;
    
    oldmode=nmode;
    
    newmode = 1; // by default, we expect the mode to change. We'll set newmode=0 explicitly when this isn't the case (rare!)
    
	if (stage < 0 || stage >=NSTAGES){

        set_param_int("error_code", ERR_INVALID_STAGE);
        nmode=MODE_ERROR;
        log_event("next_mode: stage is not a valid value %d\n",stage);
    }

	
    /* Check if this is error recovery.
    * Several things can happen:
    * 1. Error triggered COMM, which just returnd. In this case, we'll see (nmode==MODE_COMM && goodcomm!=1)
    * 2. Error handling routine decided to ignore the error and called next_mode(MODE_RESTORE)
    * 3. Error handling routine switch stage/mode, which will look just like a normal mode change.
    * Need to check for the first two cases and restore the old mode:
    */
    if ( (nmode == MODE_COMM && goodcomm != 1) || (nmode == MODE_RESTORE) )
    {
        /* just restore the previous mode */
        nmode=savemode;
        log_event("next_mode: ERROR recovery, mode stays at %d\n",nmode);
        newmode = 0; // explicitly state that this is continuation of an old mode
        return(nmode);
    }

    goodcomm=0; /* reset COMM flag */

    

	if (stage == 0)
	{
		if (nmode == MODE_START || stage != oldstage){
			log_event("STAGE 0: Main\n");
			oldstage = stage;
			icall = 0;
		}

		++icall;

		switch (icall){
		case 1:	nmode = MODE_PROFILE_DOWN; // start with going DOWN, HOME automatically
			log_event("\nNext: Down\n");
			break;
		case 2:	nmode = MODE_BOCHA;  // stabilize
            Bocha = Mission.Bocha;
			log_event("\nNext: Bocha stabilize\n");
			break;
		case 3:	nmode = MODE_XFER;  // transfer files from AUX
			log_event("\nNext: AUX transfer\n");
			break;
		case 4: nmode =  MODE_SERVO_P;  //  UP from 100m
			Servo_P = Mission.Servo_P1;
			log_event("\nNext: Servo_P Slow Up\n");
			break;
		case 5: nmode = MODE_SERVO_P;          //  slowly up 
			Servo_P = Mission.Servo_P2;
			log_event("\nNext: Servo_P Slower Up\n");
            ice_reset(); // Reset the surfacing rules
			Ice.Run = 1; // run ice sampling (in sample.c)
			break;
		case 6:  //decision! (will be either UP or DOWN)
			
			// Get real time (double check "always-surface" mode)
			time(&now);
			tm = gmtime(&now);
			yday = (double)tm->tm_yday + (double)tm->tm_hour / 24.0 + (double)tm->tm_min / 24.0 / 60.0 + (double)tm->tm_sec / 24.0 / 60.0 / 60.0;

            if (!Ice.act_now)
                log_event("WARNING: Ice.act_now was not set.\n"); // this means that the mode ended earlier than anticipated (most likely, due to Ice.DelaySec being longer than the mode duration
			if (Ice.okToSurface || yday > Ice.EpochYearDay[N_ICE_EPOCH - 1])
			{
				log_event("\nNext: UP\n");
				nmode = MODE_PROFILE_UP;
                Up = Mission.Up;
				ice_reset();
			}
			else
			{
				log_event("\nNext: Stage 0\n");
				nmode = MODE_START;
				ice_reset();
			}
			break;

		case 7: nmode = MODE_COMM; // < can we check if we timed out (i.e. stuck in ice?)
			goodcomm = 1;
			icall = 0; // after COMM, restart with case 1
			break;

		
		default: nmode = MODE_ERROR;
            set_param_int("error_code", ERR_INVALID_ICALL);
			log_event("ERROR in next_mode(), impossible combination: stage %d, icall %d\n", stage, icall);
			break;  /* can't get here */
		} // end Stage 0 loop
	}
    else if (stage == 1)
    {
        // This is error-response stage, and it is structured differently:
        // It can be called many times with MODE_START, but we must carry on

        if (stage != oldstage){ // the only way to check for /real/ start is to check oldstage
            log_event("STAGE 1: Emergency ascent\n");
            oldstage = stage;
            savetimer = Timer.enable;
            // Reset the surfacing rules
            ice_reset();
            // disable timers
            Timer.enable = 0;
            Up = Mission.EmergencyUp;
        }
        else if (nmode == MODE_START) {
            newmode = 0; // explicitly state that this is continuation of an old mode
            log_event("Continue emergency ascent\n");
        }
        
        if (nmode == MODE_START){ // first call or still in error
            nmode = MODE_PROFILE_UP; // emergency UP
        }

        else { // call at the end of MODE_UP
            nmode = MODE_START; // restart Stage 0 
            stage = 0;
            Timer.enable = savetimer; // restore Timer state
            log_event("\nNext: Stage 0\n");
        }
    }// end Stage 1
    else if (stage == 2)
    {
        // This is error-response stage, and it is structured differently:
        // It can be called many times with MODE_START, but we must carry on
        if (stage != oldstage){ // the only way to check for /real/ start is to check oldstage
            log_event("STAGE 2: Safe settle\n");
            oldstage = stage;
            savetimer = Timer.enable;
            // Reset the surfacing rules
            ice_reset();
            // disable timers
            Timer.enable = 0;
        }
        else if (nmode == MODE_START) {
            newmode = 0; // explicitly state that this is continuation of an old mode
            log_event("Continue safe settle\n");
        }

        if (nmode == MODE_START){ // first call or still in error
            Bocha = Mission.EmergencyBocha;
            nmode = MODE_BOCHA; // safe settle (B=const)
        }
        else { // call at the end of MODE_SETTLE
            nmode = MODE_START; // restart Stage 0 
            stage = 0;
            Timer.enable = savetimer; // restore Timer state
            log_event("\nNext: Stage 0\n");
        }
    }// end Stage 2	
    else if (stage == 3)
    {
        // This is a file-transfer stage, only meant to be triggered manually
        if (stage != oldstage || nmode == MODE_START || nmode == MODE_START || nmode == MODE_COMM){
            log_event("STAGE 3: AUX file transfer\n");
            oldstage = stage;
            // Reset the surfacing rules
            ice_reset();
            // disable timers
            savetimer = Timer.enable;
            Timer.enable = 0;
            nmode = MODE_XFER;
            log_event("\nNext: AUX transfer\n");
        }
        else {
            nmode = MODE_COMM;
            goodcomm = 1;
            set_param_int("comm:waiting", 1);
            Timer.enable = savetimer; // restore Timer state
            log_event("\nNext: COMM, wait\n");
        }
    }// end Stage 3


    if (newmode)
        log_event("Next_mode: %d->%d\n",oldmode,nmode);
    else
        log_event("Next_mode: continue with %d\n", nmode);
    
    savemode=nmode;
    return(nmode);
}

/************************************************************************************************************/
/*       Timer/Command/Error Exception handling functions													*/
/*	Each function returns																					*/
/*	0 if Ballast.c can proceed normally																		*/
/*	-1 if immediate return is needed																		*/
/*	1 if the current mode needs to terminate gracefully														*/
/*						(mode is set to new_mode, and next_mode is called prior to return)					*/
/************************************************************************************************************/

/* This function will handle timers */
int handle_timer(int mode, double day_time, int timer_command, int *mode_out)
{ // We can switch modes & stages based on the timer_command. 
 // Old functionality (stage switching) can be replicated by setting stage = timer_command and returning 1 to end the mode.
 // The only problem for now is signalling stage reset (but mode_out should help with that)
	int result = 0;
	if (timer_command == 1)
	{ // time to surface!
		Ice.act_now = 1; // will make the decision based on the next altimeter run!
		Ice.day_time_last = -1; // Force the altimeter run
		log_event("Setting 'act now' flag.\n");
	}
	else
	{
		log_event("Bad timer command (%d)\n", timer_command);
	}
	return result;
}

/* This function will handle commands */
int handle_command(int mode, double day_time, int command, int *mode_out)
{
	int surfcheck_result;
	// SURFCHECK command, process the new result
#ifndef SIMULATION
	surfcheck_result = get_surfcheck_result();
#else
	surfcheck_result = command; // in simulation, keep passing via command (so that the simulator could trigger it)
#endif
	return check_surfacing_rules(day_time, surfcheck_result); // will return 1 if need to terminate current mode
}

#include "SampleMIZ.c"

