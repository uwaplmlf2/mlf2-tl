/*----------------------------------------------------------- */
# if defined HURRICANEGAS

/* Hurricane Gasfloast mission
*/
/* NEED TO SET PARAMETERS AT MISSION START */

int next_mode(int nmode, double day_time)
{
    if (nmode==MODE_START){
	printf("Hurricane GAS Mission Start\n");
        /* other params untouched from Hurricane simulations */
#if defined SIMULATION 
 	printf(" SIMULATION PARAMS SET %f ",Down.Pmax);
#endif
    }
     switch(icall){
	case 0: nmode=MODE_PROFILE_DOWN;
	    Down.Pmax=20.;
	    break;
	case 1: nmode=MODE_PROFILE_UP;
		Up.Pend= 5;   /*Get to surface */
		Up.timeout=70.*60;
		break;
	case 2: nmode=MODE_PROFILE_UP;
		Up.Pend=-100;   /* end only on time  - get surface data */
		Up.timeout=70.*60;
		break;
	case 3: nmode=MODE_COMM;break;
	case 4: nmode=MODE_PROFILE_DOWN;
	    Down.Pmax=30.;
	    break;
        case 5: nmode=MODE_SETTLE;
		Settle.timeout=70.*60.;     /* first do 50 m to get away from parachutes */
		Settle.seek_time=Settle.timeout;
		break;
        case 6:nmode=MODE_PROFILE_DOWN;
		Down.Pmax=50;
		break;
        case 7: nmode=MODE_SETTLE;
		Settle.timeout=90.*60.;
		Settle.seek_time=Settle.timeout;
		break;
	case 8: nmode=MODE_PROFILE_DOWN;   /* Now do bottom part of profile */
		Down.Pmax=70;
		break;
	case 9: nmode=MODE_SETTLE;
		Settle.timeout=110.*60.;
		Settle.seek_time=Settle.timeout;
		break;
	case 10: nmode=MODE_PROFILE_DOWN;   /* Now do bottom part of profile */
		Down.Pmax=90;
		break;
	case 11: nmode=MODE_SETTLE;
		Settle.timeout=150.*60.;
		Settle.seek_time=Settle.timeout;
		break;
	case 12: nmode=MODE_PROFILE_DOWN;   /* Now do bottom part of profile */
		Down.Pmax=120;
		break;
	case 13: nmode=MODE_SETTLE;        /* final settle for V0 */
		Settle.timeout=230.*60.;
		Settle.nav=72;   /* 1 hr */
		Settle.seek_time=Settle.timeout-1.5*60.;
		break;
	case 14: nmode=MODE_PROFILE_UP; 
		Up.timeout=50000;  /* however long it takes to get to mixed layer */
		Up.Pend=12;
		break;
	case 15: nmode=MODE_DRIFT;
		break;
	case 16: nmode=MODE_PROFILE_UP;
		Up.Pend=5;   /*Get to surface*/
		Up.timeout=70.*60;
		break;
	case 17: nmode=MODE_PROFILE_UP;
		Up.Pend=-100;   /*Surface data*/
		Up.timeout=70.*60;
		break;
	case 18: nmode=MODE_COMM;
		break;
	case 19: nmode=MODE_PROFILE_DOWN;   /* Now do bottom part of profile */
		Down.Pmax=30;
		break;
	case 20: nmode=MODE_SETTLE;
		Settle.timeout=70.*60.;
		Settle.seek_time=Settle.timeout;
		break;
	case 21: nmode=MODE_PROFILE_DOWN;   /* Now do bottom part of profile */
		Down.Pmax=50;
		break;
	case 22: nmode=MODE_SETTLE;
		Settle.timeout=90.*60.;
		Settle.seek_time=Settle.timeout;
		break;
	case 23: nmode=MODE_PROFILE_DOWN;   /* Now do bottom part of profile */
		Down.Pmax=70;
		break;
	case 24: nmode=MODE_SETTLE;
		Settle.timeout=110.*60.;
		Settle.seek_time=Settle.timeout;
		break;
	case 25: nmode=MODE_PROFILE_DOWN;   /* Now do bottom part of profile */
		Down.Pmax=90;
		break;
	case 26: nmode=MODE_SETTLE;
		Settle.timeout=150.*60.;
		Settle.seek_time=Settle.timeout;
		break;
	case 27: nmode=MODE_PROFILE_DOWN;   /* Now do bottom part of profile */
		Down.Pmax=120;
		break;
	case 28: nmode=MODE_SETTLE;
		Settle.timeout=230.*60.;
		Settle.seek_time=Settle.timeout;
		break;
	case 29: nmode=MODE_PROFILE_UP;   /* Now do bottom part of profile */
		Up.Pend=2;
		break;
	case 30: nmode=MODE_DONE;break;
        default:nmode=MODE_ERROR;break;
   }
       printf("icall %d Mode %d ->",icall,nmode);
       ++icall;
       return(nmode);
   }
   #endif
