/* Sampling subroutine: for HURR09 Gas
Set float sampling scheme for this mission
Call on every data loop
May 22, 2009  Hurr09
*/
int sampling(int nmode, double day_time, double day_sec)
{
double hr, sec_hr;
int do_anr_on;  /* =1 for ANR ON time */ 
static int anr_on=0;  /* =0 if not ON now, =1 if ON now */
static double anr_on_time=0.; /* start time of current ANR ON window */
static int do_quiet;  /* want: 0- not quiet time; 1 quiet time */
static int quiet=0;     /* 0- not quiet time NOW ; 1 quiet time NOW; 2 finished quiet time */
static int quiet_cycle=0;  /* shallow-deep quiet cycle index */
static double quiet_start=0.; /* quiet start time */
static int low_power=0;   /* 1 if low power is set */

if(Steps.cycle>0){   /* power conserving - turns off , but never back on */
  if(low_power==0){
	log_event("SampleHURR09: Turn off ANR, O2 and GTD for rest of mission\n");
		/* turn off ANR */
	DISABLE_SENSORS(MODE_DRIFT_ISO, SENS_ANR);
	DISABLE_SENSORS(MODE_DRIFT_SEEK, SENS_ANR);
	DISABLE_SENSORS( MODE_DRIFT_ML, SENS_ANR);
	DISABLE_SENSORS( MODE_SETTLE, SENS_ANR);
	DISABLE_SENSORS( MODE_PROFILE_UP, SENS_ANR);
	DISABLE_SENSORS( MODE_PROFILE_DOWN, SENS_ANR);
		/* Turn off O2 sampling */
	DISABLE_SENSORS(MODE_DRIFT_ISO, SENS_O2);
	DISABLE_SENSORS(MODE_DRIFT_SEEK, SENS_O2);
	DISABLE_SENSORS( MODE_DRIFT_ML, SENS_O2);
	DISABLE_SENSORS( MODE_SETTLE, SENS_O2);
	DISABLE_SENSORS( MODE_PROFILE_UP, SENS_O2);
	DISABLE_SENSORS( MODE_PROFILE_DOWN, SENS_O2);
		/* turn off GTD sampling*/
	DISABLE_SENSORS(MODE_DRIFT_ISO, SENS_GTD);
	DISABLE_SENSORS(MODE_DRIFT_SEEK, SENS_GTD);
	DISABLE_SENSORS( MODE_DRIFT_ML, SENS_GTD);
	DISABLE_SENSORS( MODE_SETTLE, SENS_GTD);
	DISABLE_SENSORS( MODE_PROFILE_UP, SENS_GTD);
	DISABLE_SENSORS( MODE_PROFILE_DOWN, SENS_GTD);
	low_power=1;
	}
   return(0);
}

hr=floor(day_sec/3600.);
sec_hr=day_sec-hr*3600.;

/* ANR ON for 30min per hour in drift settle */
if ( (is_drift(nmode) || nmode==1) && (sec_hr>0 && sec_hr<1800 ) ){ 
	do_anr_on=1;
}
else {
	do_anr_on=0;
}

if (do_anr_on==1 && anr_on==0){  /* Should be ON */
	anr_on=1;
	anr_on_time=day_time;
	log_event("%4.2f ANR ON, mode %d\n",day_time,nmode);
	ENABLE_SENSORS(MODE_DRIFT_ISO, SENS_ANR);
	ENABLE_SENSORS(MODE_DRIFT_SEEK, SENS_ANR);
	ENABLE_SENSORS( MODE_DRIFT_ML, SENS_ANR);
	ENABLE_SENSORS( MODE_SETTLE, SENS_ANR);
}

if (do_anr_on==0 && anr_on==1){  /* Should be OFF */
	anr_on=0;
	log_event("%4.2f ANR OFF, duration %4.0fs, mode %d\n",
			day_time,(day_time-anr_on_time)*86400.,nmode);
	DISABLE_SENSORS(MODE_DRIFT_ISO, SENS_ANR);
	DISABLE_SENSORS(MODE_DRIFT_SEEK, SENS_ANR);
	DISABLE_SENSORS( MODE_DRIFT_ML, SENS_ANR);
	DISABLE_SENSORS( MODE_SETTLE, SENS_ANR);
}

/* log_event("anr %d quiet %d do_quiet %d\n",anr_on,quiet,do_quiet); */

if (0){  /* CANCEL QUIET TIME */
	/* QUIET TIMES - no pumping */
	if (anr_on==1 && quiet==0 && sec_hr>100.  ) {  /* call for quiet */
		do_quiet=1;  
	}
	if (quiet==1 && (day_time-quiet_start)*86400.>240. ) { /* finish quiet*/
		do_quiet=0;
	}
	if (anr_on==0  && quiet !=0){  
		if (quiet==1) {
			do_quiet=0.;  /* end quiet */
			}
		else {
			quiet=0;   /*reset */
		}
	}

	if (do_quiet==1 && quiet==0 &&       /* Start quiet */
		( sec_hr>1800.-900.    /* Start after 15 minutes */
		   || (PressureG<5. && quiet_cycle%2==0)  /* shallow */
		   || (PressureG>20. && quiet_cycle%2==1) /* deep */
		   )
		){
		log_event("%4.2f QUIET, P=%3.0f cycle %d mode %d\n",
			day_time,PressureG,quiet_cycle,nmode);
		quiet_start=day_time;
		quiet=1;
		++quiet_cycle;
		/* Turn off O2 sampling & thus pump noise */
		DISABLE_SENSORS(MODE_DRIFT_ISO, SENS_O2);
		DISABLE_SENSORS(MODE_DRIFT_SEEK, SENS_O2);
		DISABLE_SENSORS( MODE_DRIFT_ML, SENS_O2);
		DISABLE_SENSORS( MODE_SETTLE, SENS_O2);
		/* Turn off GTD pumping - but keep sampling */
		#ifndef SIMULATION
		set_param_double("gtd:threshold",-5.);
		set_param_double("gtd:pump_on",0.);
		set_param_double("gtd:pump_off",4000.);
		set_param_double("gtd:pump_cycles",3.);	
		#endif
	}
	if (do_quiet==0 && quiet==1 ){ /* end quiet */
		log_event("%4.2f QUIET OFF, duration %3.0f\n",day_time,
			(day_time-quiet_start)*86400.);
		quiet=2;  /* done with quiet for this ANR on time */
			/* Turn ON O2 sampling  */
		ENABLE_SENSORS(MODE_DRIFT_ISO, SENS_O2);
		ENABLE_SENSORS(MODE_DRIFT_SEEK, SENS_O2);
		ENABLE_SENSORS( MODE_DRIFT_ML, SENS_O2);
		ENABLE_SENSORS( MODE_SETTLE, SENS_O2);
		/* Restore GTD pumping - continuous  */
		#ifndef SIMULATION
		set_param_double("gtd:threshold",500.);
		set_param_double("gtd:pump_on",1000.);
		set_param_double("gtd:pump_off",3000.);
		set_param_double("gtd:pump_cycles",5.);	
		#endif
	}
}  /* CANCEL QUIET TIME */
return(0);
}
