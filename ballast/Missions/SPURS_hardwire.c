void hardwire(){
Down.Pmax = 120; //meters
Down.timeout = 3600; //seconds
Down.Sigmax = 5025.5; //
Down.B0 = 0; //
Down.Speed = 1; //
Down.Brate = 0; //
Down.drogue = 0; //0 = closed
Up.Ball = 0.00025; //
Up.Speed = 1; //
Up.Brate = 0; //
Up.Pend = 10; //must be bigger than CTD.separation/2
Up.timeout = 3600; //
Up.Speedbrake = 1; //
Up.PHyst = 10; //
Up.drogue = 0; //
Up.surfacetime = 0; //
Up2.Ball = 0.00025; //
Up2.Speed = 1; //
Up2.Brate = 0; //
Up2.Pend = 3; //
Up2.timeout = 3600; //
Up2.Speedbrake = 1; //
Up2.PHyst = 10; //
Up2.drogue = 1; //
Up2.surfacetime = 120; //
Settle.secOday = -1; //End Settle when time passes this clock time [0 86400]
Settle.timeout = 8000; //
Settle.seek_time = 7000; //
Settle.decay_time = 200; //Decay time for seeking after end of seek_time
Settle.nav = 1; //Number of points to average to get volume
Settle.drogue_time = 200; //
Settle.beta = 2; //PseudoComp (big for stable)
Settle.tau = 200; //
Settle.weight_error = 1; //Acceptable distance from target (in kg) for a good settle (first check)
Settle.Vol_error = 5e-007; //Acceptable volume error for a good settle (second check)
Settle.Ptarget = 15; //Used only with Nfake
Settle.Nfake = 0; //Fake stratification (1/s) relative to Ptarget  No effect if 0.
Settle.nskip = 1; //0 for no settle
Settle.SetTarget = 2; //Set Settle.Target at start of Settle: 1=current Potdensity, 2=Ballast.Target, 3=Drift.Target, else do not change.
Settle.Target = 1022.4; //
Settle.B0 = 0.000175687; //do not need to set
Settle.Bmin = 0; //
Settle.Bmax = 0.00065; //
Steps.time1 = 3600; //
Steps.time2 = 3600; //
Steps.time3 = 3600; //
Steps.time4 = 10000; //
Steps.z1 = 100; //
Steps.z2 = 80; //
Steps.z3 = 60; //
Steps.z4 = 50; //
Steps.cycle = 0; //
Steps.decay_ratio = 0.4; //
Psballast.depth1 = 100; //Depth of the first (captive) dive & drift
Psballast.settle1_time = 100; //length of the first (captive) settle - typically, very short
Psballast.drift1_time = 600; //length of the first (captive) drift - typically, very short
Psballast.depth2 = 100; //Depth of the second (longer) dive & drift
Psballast.settle2_time = 3600; //length of the second (longer) settle
Psballast.drift2_time = 3600; //length of the second (longer) drift. NB: Vset==1 for this drift!
Ballast.SetTarget = 8; //Set Ballast.Target to current PotDensity... 4=at good Settle end, 5=at Drift end, 6=at Down end, 7=at Up end, 8=whenever P passes Ballast.Pgoal, 9=from GetMLB (not implemented yet), else do not change it
Ballast.Vset = 1; //Set Vol0 at: 0=fixed, 1=settle, 2=settle and drift, 3=drift
Ballast.MLsigmafilt = 0; //1 to LP filter
Ballast.MLthreshold = -100; //
Ballast.MLmin = 2; //
Ballast.MLtop = 2; //
Ballast.MLbottom = 15; //
Ballast.SEEKthreshold = 8; //
Ballast.Offset = 0; //
Ballast.T0 = 26.8; //
Ballast.S0 = 37.45; //
Ballast.P0 = 0; //
Ballast.rho0 = 1025.69; //current density (used to be important)
Ballast.B0 = 160e-6; //
Ballast.V0 = 0.0485159; //Important - First guess for Settle
Ballast.TH0 = 26.8; //
Ballast.Vdev = 0; //do not set
Ballast.Pgoal = 100; //
Ballast.Target = 1022.4; //
Drift.SetTarget = 1; //Set value of Drift.Target at start of Drift: 1 = current PotDensity, 2 = Ballast.Target, 3 = Settle.Target, otherwise do not change
Drift.VoffZero = 1; //1 = set Voff=0 at drift start, else keep value
Drift.median = 1; //1 = use 5 pt median filter, 0 = do not
Drift.timetype = 2; //1 since last, 2 secondsOday, else only timeout
Drift.time_end_sec = -1; //
Drift.timeout_sec = 86500; //
Drift.Tref = 8; //
Drift.Voff = 0; //
Drift.Voffmin = -0.0002; //minimum value of Voff (bottom interaction)
Drift.Moff = 0; //
Drift.Air = 14.3e-006; //
Drift.Compress = 3.4e-006; //
Drift.Thermal_exp = 7.24e-005; //
Drift.Target = 1024.0; //Target isopycnal
Drift.iso_time = 20000; //
Drift.seek_Pmin = 250; //
Drift.seek_Pmax = 500; //
Drift.iso_Gamma = 1; //Pseudo-compressibility, 1=isopycnal
Drift.time2 = 600; //No longer used
Drift.closed_time = 720; //Drogue opens after this
Mlb.go = 1; //1=compute mlb density
Mlb.Nmin = 45; //minimum number of data to do computation
Mlb.dP = 2; //grid spacing m
Mlb.dSig = 0.02; //bin size for Sigma search
Mlb.Sigoff = 0.2; //Offset of goal from ML density, final target = MLsigma + Sigoff
Ctd.which = 2; //Which CTD to use: 0=bottom, 1=top, 2=mean,3=max
Ctd.BadMax = 25; //max # of bad before error
Ctd.Ptopmin = 2.1; //top CTD bad above this
Ctd.Poffset = 0.106; //adjusts pressure to be that at middle of float hull
Ctd.Separation = 1.42; //Poffset -CTD separation/m
Ctd.TopSoffset = 0; //
Ctd.TopToffset = 0; //
Ctd.BottomSoffset = 0; //
Ctd.BottomToffset = 0; //
Error.Modes = 3; //Creates Error if float is on surface or otherwise a bad depth range: 1=drift only, 2=settle only, 3=drift and settle, else=none
Error.Pmin = 3; //
Error.Pmax = 500; //
Error.timeout = 3600; //
Bugs.start = 3600; //sunset  / seconds of GMT day 0-86400
Bugs.stop = 40000; //sunrise
Bugs.start_weight = 0; //
Bugs.stop_weight = 0; //
Bugs.flap_interval = 10000; //time between flaps
Bugs.flap_duration = 130; //time between close and open / seconds
Timer.enable = 0; //==1 if all timers are enabled (individual timers can be controlled by setting the time ore stage to -1
Timer.time1 = 7200; //Time (in seconds since midnight) for the first timer event.
Timer.time2 = 28800; //Time (in seconds since midnight) for the second timer event.
Timer.time3 = 50400; //Time (in seconds since midnight) for the third timer event.
Timer.time4 = 72000; //Time (in seconds since midnight) for the fourth timer event.
Timer.stage1 = 2; //Stage to switch to when the first timer is triggered (set to -1 to disable)
Timer.stage2 = 1; //Stage to switch to when the second timer is triggered (set to -1 to disable)
Timer.stage3 = 1; //Stage to switch to when the third timer is triggered (set to -1 to disable)
Timer.stage4 = 2; //Stage to switch to when the fourth timer is triggered (set to -1 to disable)
Butterlow.tfilt = 40729.4; //
//
Mass0 = 50;
}
