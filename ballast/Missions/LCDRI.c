/* arch-tag: 0e0de6c5-8b15-4b25-8b35-21d785e9076f */
/*****************LCDRI***************************/
/* FROM SPURS2	*/
/* NEED TO SET PARAMETERS IN InitializeLCDRI */
/* Nov 2016  */

/* Available stages within this mission: */
#define STAGE_PSBALLAST	 0
#define STAGE_STEPS		 1
#define STAGE_BSTEPS	 2
#define STAGE_TURBULENCE 3
#define STAGE_CYCLE		 4


//#define max(a, b)	((a) > (b) ? (a) : (b))


int next_mode(int nmode, double day_time)
{
    static int icall=0; // this should be renamed "phase"
	static int next_icall = 0;

    static int oldstage=0;
    static int savemode; /* saved value of mode - except error */
    static int savetimer; /* saved value of Timer.enable */
    static int commskipcount=0;  /* Counts skipped comm modes  (not implemented in LASER) */
    static int goodcomm=1;  /* flag to set if this program set COMM mode (as opposed to the emergency COMM after ERROR)*/
    static int icycle; /* cycle counter for Profile mode */
    static double hometime=-1; /* time of last home */
    
    // backup Timer.Enable during PS ballast
	static short save_Timer_enable=-1;
   
    int oldmode, i, istep;
    double x;
    oldmode=nmode;
    
    newmode = 1; // by default, we expect the mode to change. We'll set newmode=0 explicitly when this isn't the case (rare!)
    
    
    /* Check if this is error recovery.
     * Several things can happen:
     * 1. Error triggered COMM, which just returnd. In this case, we'll see (nmode==MODE_COMM && goodcomm!=1)
     * 2. Error handling routine decided to ignore the error and called next_mode(MODE_RESTORE)
     * 3. Error handling routine switch stage/mode, which will look just like a normal mode change.
     * Need to check for the first two cases and restore the old mode:
     */
    if ( (nmode == MODE_COMM && goodcomm != 1) || (nmode == MODE_RESTORE) )
    {
        /* just restore the previous mode */
        nmode=savemode;
        log_event("next_mode: ERROR recovery, mode stays at %d\n",nmode);
        newmode = 0; // explicitly state that this is continuation of an old mode
        return(nmode);
    }
    
	/* Check if this is the start of a stage (do it centrally now!)
	 * Note that a new stage should start at the beginning. No need to check for new stage below, just check for MODE_START.
	 */
	if (nmode == MODE_START || stage != oldstage) {
		log_event("*** STAGE %d ***\n", stage);
		oldstage = stage;
		nmode = MODE_START;
		icall = 1;
	}
	// Update the state - just the stage and phase(icall) for now
	SET_STATE(stage,icall,0,0); 

	next_icall = icall + 1; // by default, plan to proceed to the next phase; in some cases this can be changed to skip/rewind phases

    goodcomm=0; /* reset COMM flag */
    
	// disable SSAL by default, to be requested below for specific modes
	SSAL.Run = 0;

    switch (stage){
        case STAGE_PSBALLAST:
            /* --------------------- PS BALLAST STAGE ---------------   */
			if (nmode == MODE_START) {
				/* Set  HOME at end of DOWN  */

				set_param_int("down_home", 1);
				// Disable timers for PS ballast:
				if (save_Timer_enable == -1){ // need to check this, because there may be multiple calls with MODE_START
					save_Timer_enable = Timer.enable;
				Timer.enable = 0;
				}
            }

            switch(icall){
			case 1: nmode = MODE_PROFILE_DOWN;  // Initial captive dive  - short 
                    Prof = PSBallast.Down[0];
                    break;
                case 2: nmode=MODE_SETTLE; // First (captive) settle
					Settle = PSBallast.Settle[0];
                    break;
                case 3: nmode=MODE_DRIFT_SEEK; // First (captive) drift
					Drift = PSBallast.Drift[0];
                    break;
				case 4: nmode = MODE_PROFILE_UP;
					Prof = PSBallast.Up;
                    break;
                case 5: nmode=MODE_COMM;
                    goodcomm=1;
                    log_event("next_mode:COMM after test dive\n");
                    break;
                    
                    /**** Actual ballast - longer settle */
				case 6: nmode = MODE_PROFILE_DOWN;
					Prof = PSBallast.Down[1];
                    hometime=day_time; // Record this home //??? It homes *after* this down (at best!), not now!
                    break;
                case 7:
                    nmode=MODE_SETTLE;
					Settle = PSBallast.Settle[1];
                    break;
                case 8: nmode=MODE_DRIFT_SEEK;
					Drift = PSBallast.Drift[1];
                    break;
				case 9: nmode = MODE_PROFILE_UP;
					Prof = PSBallast.Up;
                    // enable timers:
                    Timer.enable = save_Timer_enable;
                    stage = STAGE_STEPS;  /* continue as STEPS */
                    log_event("End of PS ballast.\n");
                    break;
                default:
                    goto Bad_icall; // will throw ERR_INVALID_ICALL
            }
            break; /* End STAGE_PSBALLAST loop */
        case STAGE_STEPS:
            /* ------------------ STEPS STAGE --------------  */
            switch(icall){
                case 1: nmode=MODE_PROFILE_UP; //UP
                    Prof = Steps.Up;
					SSAL.Run = 0.;
                    break;
                case 2: nmode=MODE_COMM;
                    goodcomm=1;
                    break;
				case 3: nmode = MODE_PROFILE_DOWN;
					Prof = Steps.Down;
					if (Steps.SetFromDepth) {
						// We will need to infer density targets for settling from the density profile...
						// First, verify that the down is deep enough:
						for (i = 0; i <= 3; i++) {
							if (Prof.Ptarget <= Steps.Settle[i].Ptarget) {
								log_event("WARNING: down.Pmax (%6.3f) <= Steps.Settle[%d].Ptarget (%6.3f), ", Prof.Ptarget, i + 1, Steps.Settle[i].Ptarget);
								Prof.Ptarget = Steps.Settle[i].Ptarget + 1;
								log_event("extending to %6.3f\n", Prof.Ptarget);
							}
						}
						Mlb.point = 0;  /* initialize recorder */
						Mlb.record = 1;  /* Start recording */
						log_event("Start Recording downcast \n");
					}

                    if (hometime<0 || day_time-hometime > Home_days){   // Home on this down
                        set_param_int("down_home",  1);
                        hometime=day_time; // remember the time
                    }
                    else {
                        set_param_int("down_home", -1); // do not home
                    }
                    
                    
                    break;
                case 4: nmode=MODE_SETTLE;   /* Settle #1 */
					if (Mlb.record) {
						Mlb.record = 0; // Stop recording
						log_event("Finish Recording downcast\n");
						pMlb = &Mlb;
					}
                    // continue, do not break yet!
                case 5:/* Settle #2 */
                case 6:/* Settle #3 */
                case 7:/* Settle #4 */
                    istep = icall-4;
                    Settle = Steps.Settle[istep];

					if (Steps.SetFromDepth) {
						/* Now, infer density targets for settling from the saved density profile...

						 NB: there's a similar mechanism in ballast.c to set Ballast.Target from density at a given depth (activated with Ballast.SetTarget==8,
						 but it only works for a single depth. This is a more advanced algorithm that remembers the profile (but it needs the profile in the first place!)
						 Eventually, the two should probably be merged. -AS
						 */
						x = z2sigma(pMlb, Settle.Ptarget);  /* Get new target from saved profile */

						if (x > 0) {
							Settle.Target = x;
							log_event("Setting Settle Target #%d: %6.3f db %6.3f sigma\n", istep + 1, Settle.Ptarget, x - 1000);
						}
						else {    /* if z2sigma() fails use Ballast.Target from initial ballasting */
							log_event("ERROR: next_mode, no new target #%d, use %6.3g\n", istep + 1, Ballast.Target - 1000.);
						}
					}
                    if (icall==7){
                        next_icall = 1; // go to the beginning after the last step
                    }
                    break;
                default:
                    goto Bad_icall; // will throw ERR_INVALID_ICALL
            }
            break;/* End STAGE_STEPS loop */
		case STAGE_BSTEPS:
			switch (icall) {
			case 1: nmode = MODE_PROFILE_UP;
				Prof = BSteps.Up;
				SSAL.Run = 0;
				break;
			case 2: nmode = MODE_COMM;
				goodcomm = 1;
				break;
			case 3: nmode = MODE_PROFILE_DOWN;
				Prof = BSteps.Down;
				if (hometime < 0 || day_time - hometime > Home_days) {   // Home on this down
					set_param_int("down_home", 1);
					hometime = day_time; // remember the time
				}
				else {
					set_param_int("down_home", -1); // do not home
				}
				break;
			case 4:/* Settle #1 */
			case 5:/* Settle #2 */
			case 6:/* Settle #3 */
			case 7:/* Settle #4 */
				nmode = MODE_BOCHA;
				istep = icall - 4;
				Bocha = BSteps.Bocha[istep];
				if (icall == 7) {
					next_icall = 1; // go to the beginning after the last step
				}
				break;
			default:
				goto Bad_icall; // will throw ERR_INVALID_ICALL
			}
			break;/* End STAGE_BSTEPS loop */
            
        case STAGE_TURBULENCE:
  
            switch(icall){
			case 1: nmode = MODE_PROFILE_UP;
                    Prof = Turb.Up;
					SSAL.Run = 0;
                    break;
                case 2: nmode=MODE_COMM;
                    goodcomm=1;
                    break;
				case 3: nmode = MODE_PROFILE_DOWN;
                    Prof = Turb.Down;
                    if (hometime < 0 || day_time-hometime > Home_days){   // Home on this down
                        set_param_int("down_home",  1);
                        hometime=day_time; // remember the time
                    }
                    else {
                        set_param_int("down_home", -1); // do not home- This is preferred
                    }
                    break;
				case 4: nmode = MODE_PROFILE_UP;  // This is the special Insertion UP
                    Prof = Turb.InsUp;
                    break;
                case 5: nmode=MODE_DRIFT_ML;
					Drift = Turb.Drift;
                    // go back to the beginning next
                    next_icall = 1;
                    break;
                default:
                    goto Bad_icall; // will throw ERR_INVALID_ICALL
            }
            break;/* end STAGE_TURBULENCE loop */
            
        case STAGE_CYCLE:
            /* -----------------   RAPID-PROFILING (CYCLE) STAGE  ----------------------- */
            if (nmode == MODE_START) {
                icycle = 0; // count the number of cycles
            }
            switch (icall) {
                case 1: 
STAGE_CYCLE_1:
					nmode = MODE_PROFILE_DOWN;
					Prof = Cycle.Down;
					++icycle;
	                 if (hometime < 0 || day_time - hometime > Home_days) {   // Home on this down
                        set_param_int("down_home", 1);
                        hometime = day_time; // remember the time
                    }
                    else {
                        set_param_int("down_home", -1); // do not home - preferred
                    }
	                break;
                case 2: nmode=MODE_PROFILE_UP;
                    Prof = Cycle.Up;
					SSAL.Run = 0;
                    break;
                case 3:
                    if (icycle==Cycle.ncycles){ // Do COMM this time
                        nmode=MODE_COMM;
                        goodcomm=1;
                        icycle=0;
                        next_icall = 1; // then rewind normally
                    }
                    else {       // Skip COMM this time
						log_event("Cycle# %d - skip COMM\n", icycle);
                        /* rewind now, without exit*/
                        next_icall = 2;
						goto STAGE_CYCLE_1;
                    }
                    break;
                    
                default:
                    goto Bad_icall; // will throw ERR_INVALID_ICALL
            }
            break;/* end STAGE_TURBULENCE loop */
            

        default:/* BAD STAGE */
            nmode=MODE_ERROR;
            log_event("ERROR in next_mode(): invalid stage %d\n",stage);
            set_param_int("error_code", ERR_INVALID_STAGE);
    } /* end stage case*/
    
    if (newmode)
        log_event("Next_mode: %d->%d\n",oldmode,nmode);
    else
        log_event("Next_mode: continue with %d\n", nmode);
    
	SET_STATE_MODE(nmode);
	savemode=nmode;
	icall = next_icall; // set-up the phase of the next run
    return(nmode);

    /* Handle the bad icall exception here: */
Bad_icall:
    nmode = MODE_ERROR;
    log_event("ERROR in next_mode(): impossible combination: stage %d, icall %d\n", stage, icall);
    set_param_int("error_code", ERR_INVALID_ICALL);
	// Update the state
	SET_STATE_MODE(nmode); // Update the state
    return(nmode);
}

/************************************************************/
/*       Timer/Command/Error Exception handling functions	*/
/* Keep this functionality, although it does little in this mission */
/*														*/		
/*	Each function returns								*/
/*	0 if Ballast.c can proceed normally					*/
/*	-1 if immediate return is needed					*/
/*	1 if the current mode needs to terminate gracefully	*/
/*		(mode is set to *mode_out, and next_mode is called prior to return)	*/
/************************************************************/

/* This function will handle timers */
int handle_timer(int mode, double day_time, int timer_command, int *mode_out)
{
	if (timer_command >= 0) {
		stage = timer_command; // Set new stage 
		*mode_out = MODE_START; // Force the stage restart
		log_event("Timer activated. End mode %d New Stage %d\n", mode, stage);
		return (1); /* returning 1 to gracefully end mode */
	}
	else 	{
		log_event("Got timer command %d Do nothing\n", timer_command);
		return (0);
	}
}

/* This function will handle commands */
int handle_command(int mode, double day_time, int command, int *mode_out)
{
    log_event("Got command %d  Do nothing\n\n",command);
    return(0);
}

#include "SampleLCDRI.c"

