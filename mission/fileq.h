/*
** arch-tag: 48c52e51-f9cc-4b93-a852-381d8011c513
** Time-stamp: <2015-03-30 11:15:05 mike>
*/
#ifndef _FILEQ_H_
#define _FILEQ_H_

#define FQ_DUMP_FILE    "fileq.txt"

void fq_init(void);
int fq_add(const char *file);
int fq_add_next(const char *file);
int fq_send(int (*fsend)(const char*), int keep);
int fq_len(void);
int fq_add_uncompressed(const char *file);
int fq_dump(const char *filename);
int fq_load(const char *filename);

#endif /* _FILEQ_H_ */
