/*
** arch-tag: macros to temporarily change cpu speed
*/
#ifndef _CPUCLOCK_H_
#define _CPUCLOCK_H_

#include "serial.h"

/*
 * The following macros should be used to bracket a region in which the
 * CPU clock speed needs to be temporarily changed.  Note that these
 * macros form a block so the enclosed region must not cross an existing
 * block boundary.
 */
#define CPU_SET_SPEED(s) {\
                             long __fsys, __baud;\
                             __baud = SerGetBaud(0L, 0L);\
                             __fsys = SimGetFSys();\
                             SimSetFSys(s);\
                             SerSetBaud(__baud, 0L);	\
                             serial_reset_speed();	\
			     DelayMilliSecs(50L)

#define CPU_RESET_SPEED()    SimSetFSys(__fsys);\
                             serial_reset_speed();\
                             SerSetBaud(__baud, 0L);}

#define CPU_RESET_AND_RETURN(x)	SimSetFSys(__fsys);\
                                serial_reset_speed();\
                                SerSetBaud(__baud, 0L);\
                                return x
#endif /* _CPUCLOCK_H_ */
